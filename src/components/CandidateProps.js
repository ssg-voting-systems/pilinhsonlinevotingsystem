import React from 'react';
import	{Row, Col, Card, Button} from "react-bootstrap";
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

export default function CandidateProps({candidateProp}){

	const { _id, firstName, lastName, middleName} = candidateProp;
	// const { _id, firstName, lastName, middleName, position, picPath, grade, section} = candidateProp;
	console.log(candidateProp)
	// const [count, setCount] = useState(0);
		// const [seats,setSeats] = useState(30);
		// const [isOpen, setIsOpen] = useState(true);

		// const enroll = () =>{	
		// 		setSeats(seats - 1);
		// 		setCount(count + 1)	
		// }

		// useEffect(()=> {
		// 	if (seats ===0){
		// 		setIsOpen(false);
		// 	}
		// }, [seats])
return (
		<Row className="pt-3">
			<Col xs={12} md={6}>
				<Card className="courseCard p-3">
					<Card.Body>
						<Card.Title><h3>{ `${firstName} ${middleName} ${lastName}` }</h3></Card.Title>
						<br/>			
						<Card.Subtitle><h5>{firstName}</h5></Card.Subtitle>
						{/*<Card.Text>{ position }</Card.Text>*/}
						<Card.Subtitle><h5>Grade & Section:</h5></Card.Subtitle>
						{/*<Card.Text>&#8369; {grade }{ section }</Card.Text>*/}
						{/*<Link to={`/products/${_id}`}><Button className="btn">Read More details</Button> </Link>*/}
						{/*<Link to={`/VotingPage`}><Button className="btn" onClick={() => { setVicePresidentLRN(_id)}} >Vote</Button></Link>*/}
					</Card.Body>

				</Card>
			</Col>
		</Row>
	)

}

CandidateProps.propTypes = {
	candidateProp: PropTypes.shape({
		firstName: PropTypes.string.isRequired,
		lastName: PropTypes.string.isRequired,
		middleName: PropTypes.string.isRequired
		// grade: PropTypes.number.isRequired,
		// section: PropTypes.string,
		// position: PropTypes.string
		// picPath: PropTypes.string.isRequired,
		// price: PropTypes.number.isRequired
		// photo: PropTypes.string.isRequired
	})
}
