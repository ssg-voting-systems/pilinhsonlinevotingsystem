import React, {useState} from 'react';
import {UserProvider} from './UserContext';
import  {Container} from "react-bootstrap";


import './App.css';
import AppNavbar from './components/AppNavBar';
import Home from'./pages/Home';
// import Register from './pages/Register'
// import Login from './pages/Login'
import Logout from './pages/Logout'
import PageNotFound from './pages/PageNotFound'
import Footer from './components/Footer';
import President from './components/President';
import VicePresident from './components/VicePresident';
import Secretary from './components/Secretary';
import Treasurer from './components/Treasurer';
import Auditor from './components/Auditor';
import PIO from './components/PIO';
import ProtocolOfficer from './components/ProtocolOfficer';
import GLR from './components/GLR';
import AdminView from './pages/AdminView';


// for Routes
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

function App() {

  const [user, setUser] = useState({
      accessToken: localStorage.getItem('accessToken'),
      email: localStorage.getItem('email'),
      isAdmin: localStorage.getItem('isAdmin') === 'true'
  })

  const unsetUser = () => {
      localStorage.clear()
  }

  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container>
            <Routes>
              <Route path="/home" element={<Home />} />
              <Route path="/" element={<Home />} />
              <Route path='/logout' element={<Logout />} />
              <Route path='*' element={<PageNotFound />} />
              <Route path='/president' element={<President />} />
              <Route path='/vicepresident' element={<VicePresident />} />
              <Route path='/secretary' element={<Secretary />} />
              <Route path='/treasurer' element={<Treasurer />} />
              <Route path='/auditor' element={<Auditor />} />
              <Route path='/pio' element={<PIO />} />
              <Route path='/protocolOfficer' element={<ProtocolOfficer />} />
              <Route path='/glr' element={<GLR />} />
              <Route path='/AdminView' element={<AdminView />} />
         
          {/*    <Route path='/register' element={<Register />} />
              <Route path='/login' element={<Login />} />
              
              <Route path'/logout' element={<Logout />} />
              <Route path='/menu' element={<Menu />} />
              <Route path='/myCart' element={<Cart />} />
              <Route path='/myAccount' element={<UserDashboard />} />
              <Route path='/products/:productId' element={<SpecificProduct />} />}
              */}
            </Routes>
        </Container>  
        <Footer /> 
      </Router>
    </UserProvider>

  );
}

export default App;

document.body.style.backgroundColor = "#4cc9f0";